# Search for W to gamma pi decays

This repository holds a jupyter notebook to get started with the summer student project
on the search for rare W decays using top pair events as a trigger.

The notebook is expected to run using SWAN. 
To get started you can either click here:

[![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/psilva/wgammapi.git)

for an automated start, or do it by hand with the following steps

1. go to swan.cern 
1. open a terminal in the web page
1. clone this repository `git clone https://gitlab.cern.ch/psilva/wgammapi.git`
1. open the `WgammapiSelector.ipynb` notebook from the webpage

Note: In either case please use the `Bleeding edge` software stack