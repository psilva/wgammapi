import os

#a dict with datasets of interest
datasetDirectories={
    'MC2017_TTbar_WpLnu_Wmgpi':'/eos/cms/store/cmst3/group/top/WGammaPi/TTbar_WplusToLNu_WminusToGammaPi_8TeV_TuneCUETP8M1',
    'MC2017_TTbar_WmLnu_Wpgpi':'/eos/cms/store/cmst3/group/top/WGammaPi/TTbar_WminusToLNu_WplusToGammaPi_8TeV_TuneCUETP8M1',
    'MC2017_TTGJets':'/eos/cms/store/mc/RunIISummer16NanoAODv7/TTGJets_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8/NANOAODSIM/PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/120000',
    'Data2016B_SingleMu':'/eos/cms/store/data/Run2016B/SingleMuon/NANOAOD/02Apr2020_ver2-v1/',
    'Data2016C_SingleMu':'/eos/cms/store/data/Run2016C/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2016D_SingleMu':'/eos/cms/store/data/Run2016D/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2016E_SingleMu':'/eos/cms/store/data/Run2016E/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2016F_SingleMu':'/eos/cms/store/data/Run2016F/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2016G_SingleMu':'/eos/cms/store/data/Run2016G/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2016H_SingleMu':'/eos/cms/store/data/Run2016H/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2017B_SingleMu':'/eos/cms/store/data/Run2017B/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2017C_SingleMu':'/eos/cms/store/data/Run2017C/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2017D_SingleMu':'/eos/cms/store/data/Run2017D/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2017E_SingleMu':'/eos/cms/store/data/Run2017E/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2017F_SingleMu':'/eos/cms/store/data/Run2017F/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2018A_SingleMu':'/eos/cms/store/data/Run2018A/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2018B_SingleMu':'/eos/cms/store/data/Run2018B/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2018C_SingleMu':'/eos/cms/store/data/Run2018C/SingleMuon/NANOAOD/02Apr2020-v1/',
    'Data2018D_SingleMu':'/eos/cms/store/data/Run2018D/SingleMuon/NANOAOD/02Apr2020-v1/'
}


def listDatasets():
    
    """ prints the available keys """

    print('The following datasets are available:',list(datasetDirectories.keys()))
    
    return

def getFilesFor(sample_dir):
    
    """looks recursively to the sub-directories and fills with a list of files to use"""

    fileList=[]
    for x in os.listdir(sample_dir):
        url=os.path.join(sample_dir,x)
        if os.path.isdir(url):
            fileList += getFilesFor(url)
        else:
            fileList += [url]

    return fileList

def getFilesForDataset(dataset):

    """ lists the files in a dataset directory and retrieves a list of files for analysis """

    try:
        sample_dir=datasetDirectories[dataset]
        fileList=getFilesFor(sample_dir)
    except:
        print('No directories available for dataset=',dataset,'returning an empty list')
        fileList=[]

    return fileList
