#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"

#include <vector>

using namespace ROOT::VecOps;
using RNode = ROOT::RDF::RNode;
using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;
using rvec_b = const RVec<bool>;

//
rvec_b crossClean(const rvec_f &eta1,const rvec_f &phi1, const rvec_f &eta2, const rvec_f &phi2,float coneSize=0.4)
{
    size_t nobjs(eta1.size());
    std::vector<bool> isIso(nobjs,false);
    
    size_t ntestobjs(eta2.size());
    
    //loop over the first list of objects
    for(size_t i=0; i<nobjs; i++) {
        
        //find the min. distance with respect to the second list of objects
        float minDR(9999.);
        for(size_t j=0; j<ntestobjs; j++) {         
            float dR=DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
            if(dR>minDR) continue;
            minDR=dR;
        }
        
        //if above the required cone size the object is isolated
        if(minDR>coneSize) isIso[i]=true;
    }

    return rvec_b(isIso.begin(), isIso.end());
}


//
rvec_i matchObjects(const rvec_f &eta1,const rvec_f &phi1, const rvec_f &eta2, const rvec_f &phi2,float coneSize=0.1)
{
    size_t nobjs(eta1.size());
    std::vector<int> matchIdx(nobjs,-1);
    
    size_t ntestobjs(eta2.size());
    
    //loop over the first list of objects
    for(size_t i=0; i<nobjs; i++) {
        
        //find the first object within the required cone size and assign its index
        for(size_t j=0; j<ntestobjs; j++) {         
            float dR=DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
            if(dR>coneSize) continue;
            matchIdx[i]=j;
            break;
        }
    }

    return rvec_i(matchIdx.begin(), matchIdx.end());
}

//
rvec_b selectGenLevel(const rvec_i &pdgId, const rvec_i &statusFlags, const rvec_i &genPartIdxMother) 
{
    size_t nobjs(pdgId.size());
    std::vector<bool> isOfInterest(nobjs,false);
 
    
    //loop over the list of candidates and find photons and pions from the W decays
    for(size_t i=0; i<nobjs; i++) {
        if( abs(pdgId[i])!=22 && abs(pdgId[i])!=211 ) continue;
        
        bool isPrompt( statusFlags[i] & 0x1 );
        if(!isPrompt) continue;
        
        bool isFromHardProcess( (statusFlags[i]>>8) & 0x1 ); 
        if(!isFromHardProcess) continue;
        
        int motherIdx=genPartIdxMother[i];
        if(motherIdx<0 || motherIdx>nobjs) continue;
        if(abs(pdgId[motherIdx])!=24 ) continue;
        
        isOfInterest[i]=true;        
    }
    

    return rvec_b(isOfInterest.begin(),isOfInterest.end());    
}


//
rvec_f buildGammaPiCandidates(const rvec_f &trk_pt, const rvec_f &trk_eta, const rvec_f &trk_phi,
                              const rvec_f &a_pt,   const rvec_f &a_eta,   const rvec_f &a_phi)
{
    const float pimass(0.13957018);
    
    std::vector<float> api_cands; //(trk_pt.size()*a_pt.size());
    
    //loop over isolated tracks and interpret them as charged pions
    for(size_t i=0; i<trk_pt.size(); i++) {
        
           ROOT::Math::PtEtaPhiMVector chpi(trk_pt[i], trk_eta[i], trk_phi[i], pimass);        

           //loop over the photons and built the total 4-momentum of the pair
           for(size_t j=0; j<a_pt.size(); j++) {
               
               ROOT::Math::PtEtaPhiMVector a(a_pt[j], a_eta[j], a_phi[j], 0.);  
               //api_cands[api_cands.size()]=(a+pi).mass();               
               api_cands.push_back( (a+chpi).mass() );

           }
    }
    
    return rvec_f(api_cands.begin(), api_cands.end());
}
